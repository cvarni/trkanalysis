#!/usr/bin/env python
from glob import glob
import sys
from argparse import ArgumentParser

from AthenaCommon.Configurable import Configurable
from AthenaConfiguration.AllConfigFlags import ConfigFlags

# Set up logging and new style config
Configurable.configurableRun3Behavior = True

# Argument parsing
parser = ArgumentParser(description='Parser for TRKAnalysis configuration')
parser.add_argument("-i", "--input",
                    help="The input file to use")
parser.add_argument("-o", "--output", default="TRKNtuple.root",
                    help="The output file to use")
parser.add_argument("-n", "--maxEvents", default=-1, type=int,
                    help="The number of events to run. -1 runs all events.")
parser.add_argument("--doHitInfo", help='also save hit variables', action='store_true', default=False)
parser.add_argument("--disableDecoration", help='disable extra track and truth decoration if possible', action='store_true', default=False)

args = parser.parse_args()

# Configure
ConfigFlags.Input.Files = []
for path in args.input.split(','):
    ConfigFlags.Input.Files += glob(path)
ConfigFlags.Output.HISTFileName = args.output
ConfigFlags.PhysVal.IDPVM.runDecoration = not args.disableDecoration

# Construct our accumulator to run
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
acc = MainServicesCfg(ConfigFlags)
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
acc.merge(PoolReadCfg(ConfigFlags))

ConfigFlags.lock()

from TRKAnalysis.TRKNtupleAlgConfig import TRKNtupleAlgCfg
acc.merge(TRKNtupleAlgCfg(ConfigFlags, IsHitInfoAvailable=args.doHitInfo))

# Execute and finish
sc = acc.run(maxEvents=args.maxEvents)

# Success should be 0
sys.exit(not sc.isSuccess())
