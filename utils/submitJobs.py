import os
import sys

suffix=".TRKNtuple_220111"
username="tstreble"

dsList=open("mc_samples.txt",'r')
lines=dsList.readlines()

def submitJob(ds) :
    com = "pathena --trf \"runTRKAnalysis.py -i %IN -o %OUT.TRKNtuple.root\" --mergeOutput "

    com += "--inDS " + ds + " "
    oDS = "user."+username+"."+ds.replace("/","")+suffix

    print(oDS+"  has length: "+str(len(oDS)))
    while len(oDS) > 115 :
        print(len(oDS)," too long!!!")
        splODS=oDS.split("_")
        splODS.pop(2)
        oDS="_".join(splODS)
        pass
    print("final: "+oDS+"  has length: "+str(len(oDS)))
    com += "--outDS "+ oDS + " "
  
    com += "--destSE IN2P3-CPPM_SCRATCHDISK "
    
    return com



for ds in lines :
    if "#" in ds : continue    
    print(" ")
    print("///////////////////////////////////////////////////////////////////////////////////////")
    print(ds)
    command=submitJob(ds.replace("\n",""))
    print(command)
    os.system(command)
    pass

